protobuf-drupal7
================

[Protobuf](https://developers.google.com/protocol-buffers/) support of Obiba objects for Drupal7, based on [Protobuf-PHP](https://github.com/drslump/Protobuf-PHP).

Note: see [repository on drupal.org](https://drupal.org/project/obiba_protobuf/).
